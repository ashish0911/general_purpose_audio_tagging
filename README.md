####### General Purpose Audio Tagging

The model in this project classifies different audio sound to 41 categories of sounds like laughter, firework, meow, cough ect.

The order in which the program should run is as follows:-

- trimWavs.py
- melSpect.py
- sumFeature.py
- pca.py
- stratFold.py
- melTrain.py
- pcaMelTrain.py
- test.py

The data must be downloaded from Kaggle Freesound datasets(https://www.kaggle.com/c/freesound-audio-tagging) and stored in folder named ``data``
